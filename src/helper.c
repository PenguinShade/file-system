#include "helper.h"

//Maybe we don't need this here and only in header?
// MINODE *root;
// MINODE minode[NMINODE];
// MINODE *root;
// int fd, dev, nblocks, ninodes, bmap, imap, inode_start;

int get_block(int fd, int blk, char buf[])
{
    lseek(fd, (long)blk * BLKSIZE, 0);
    read(fd, buf, BLKSIZE);
}

int put_block(int fd, int blk, char buf[])
{
    lseek(fd, (long)blk * BLKSIZE, 0);
    write(fd, buf, BLKSIZE);
}

int tokenize(char *line, char *names[])
{
    int i = 0;
    char *s;
    s = strtok(line, "/"); // first call to strtok()
    names[i] = s;
    i++;

    while (s)
    {
        s = strtok(0, "/");
        names[i] = s;
        i++;
    }
    return i - 1;
}

int tokenizePath(char *pathname, char *name[])
{
    // char buf[1024];
    // strcpy(buf, pathname);
    char *cp;
    int i = 0;
    cp = strtok(pathname, "/");
    while (cp != 0)
    {
        name[i] = cp;
        cp = strtok(NULL, "/");
        ++i;
    }
    nameArrayLength = i;
    return 1;
}
/*!
    Returns the MINODE given the device (fd) and inode number    
*/
MINODE *iget(int dev, int ino)
{
    MINODE *mip;
    int i = 0;
    // return minode pointer to loaded INODE
    for (i = 0; i < NMINODE; i++)
    {
        mip = &minode[i];
        if (mip->refCount > 0 && mip->dev == dev && mip->ino == ino) //mip->refCount > 0 && 
        {
            mip->refCount++; //How many places in code the minode is possibly used (iput lowers reference)
            return mip;
        }
    }
    for (i = 0; i < NMINODE; i++)
    {
        mip = &minode[i];
        if (mip->refCount == 0)
        {
            mip->refCount = 1;
            mip->dev = dev;
            mip->ino = ino;
            break;
        }
    }
    int blk, offset;
    char buf[BLKSIZE];
    blk = (ino - 1) / 8 + inode_start;
    offset = (ino - 1) % 8;

    get_block(dev, blk, buf);
    ip = (INODE *)buf + offset;
    mip->INODE = *ip; // copy INODE to mp->INODE
    return mip;
}

//Writes the inodes data modifications back to disk and disposes from cache
int iput(MINODE *mip) // dispose a used minode by mip
{
    int blk, ino, offset;
    char iblk[BLKSIZE];
    mip->refCount--;

    // if (mip->refCount > 0)
    //         return 0;
    if (!mip->dirty)
        return 0;

    ino = mip->ino;
    blk = (int)((ino - 1) / 8 + gp->bg_inode_table); // disk block contain this INODE
    offset = (ino - 1) % 8;            // offset of INODE in this block
    get_block(mip->dev, blk, ibuf);
    ip = (INODE *)ibuf + offset; // ip old memory, may have been updated since
    *ip = mip->INODE;
    put_block(mip->dev, blk, ibuf);
    return 1;
}

//This returns the ino of the corresponding file path
//Also ip should be loaded to root
//Returns 0 if no inode is found
int getIno(char *pathname)
{
    INODE *ip;
    int ino = 0, blk, offset, i;
    int iblk = gp->bg_inode_table; //inodes_start_block number
    printf("iblk num: %u\n", iblk);
    if (pathname[0] == '/')
        ip = &root->INODE;
    else
        ip = &running->cwd->INODE;
    if (TEST)
        printf("Tokenizing\n");
    tokenizePath(pathname, name);
    if (nameArrayLength == 0 && ip == &root->INODE) //It's just '/' in pathname
    {
        //return root ino
        return 2;
    }
    if (TEST)
        printf("NameArrayLength: %d\n", nameArrayLength);
    for (i = 0; i < nameArrayLength; i++)
    {
        if (TEST)
            printf("name[%d]: %s\n", i, name[i]);
        ino = search(ip, name[i]);
        if (TEST)
            printf("Inode found: %d\n", ino);
        if (ino == 0)
        {
            printf("can't find %s\n", name[i]);
            return 0;
            // exit(1);
        }
        // Mailman's algorithm: Convert (dev, ino) to inode pointer
        if (TEST)
            printf("Comput block numbers\n");
        blk = (ino - 1) / 8 + iblk; // disk block contain this INODE
        offset = (ino - 1) % 8;     // offset of INODE in this block
        get_block(dev, blk, ibuf);
        ip = (INODE *)ibuf + offset; // ip -> new INODE
    }
    return ino;
}

//initializes a new MINODE that is to be added under parent with the enter_child call
MINODE *insert_new_minode(MINODE *parent, char *childPath, int mode, int gparentIno)
{
    if (TEST)
        printf("inside insert_new_minode\n");

    //get system time
    time_t mytime = time(NULL);

    int ino, blk;
    MINODE *mip;
    char childName[256];

    strcpy(childName, __xpg_basename(childPath));

    if (TEST)
        printf("iallocing,\t");
    ino = ialloc(dev);

    // if (TEST)
    //     printf("ballocing\n");
    //blk = balloc(dev);

    mip = iget(dev, ino);
    //mip->INODE.i_block[0] = blk;
    mip->INODE.i_size = 0;
    if(mode == (DIRTYPE | DIR_PERM))
        mip->INODE.i_size = BLKSIZE;
    mip->INODE.i_mode = mode;
    mip->INODE.i_uid = running->uid;
    mip->INODE.i_gid = running->gid;
    mip->INODE.i_ctime = mytime;
    mip->INODE.i_atime = mytime;
    mip->INODE.i_mtime = mytime;
    mip->INODE.i_links_count = 2;
    mip->refCount += 1;
    mip->dirty = 1;

    iput(mip);
    if(strcmp(childName, "..") == 0){
        ino = gparentIno;
    }
    if (!enter_child(parent, ino, childName))
    {
        printf("failed to enter child under parent\n");
        return 0;
    }
    if (TEST)
        printf("Child successfully entered\n");
    parent->dirty = 1;
    iput(mip);
    return mip;
}

int enter_child(MINODE *parent, int childIno, char *childName)
{
    //FIXME: Doesn't support indirect blocks
    if (TEST){
        printf("attempting to enter child\n");
        printf("%s ino: %d\n", childName, childIno);
    }

    int fileSize, blk, lblk, remainingBlockSpace, totalBlocks, i;
    char buf[BLKSIZE], *cp;
    DIR_ENTRY *dp;

    parent->dirty = 1;
    fileSize = parent->INODE.i_size;
    totalBlocks = getTotalBlocks(parent);
    if (totalBlocks == 0) //Completely empty parent minode
    {
        blk = balloc(dev);
        parent->INODE.i_block[0] = blk;
        cp = buf;
        dp = (DIR_ENTRY *)cp;
        dp->file_type = 0; //FIXME: no idea
        dp->inode = childIno;
        dp->name_len = strlen(childName);
        strncpy(dp->name, childName, dp->name_len);
        dp->rec_len = BLKSIZE; //Set to entire block because only entry
        put_block(dev, blk, buf); //Update the block in memory
        return 1;
    }
    lblk = totalBlocks - 1;

    // for (i = 0; i < totalBlocks; i++)
    // {
    blk = parent->INODE.i_block[lblk]; //ONLY WORKS FOR SINGLE BLOCKS (not indirect)
    get_block(dev, blk, buf);
    dp = (DIR_ENTRY *)buf;
    cp = buf;
    while (cp < buf + BLKSIZE) //While valid directory structure and not outside block
    {
        if ((dp->rec_len + cp) >= (buf + BLKSIZE)) //If we are looking at the last entry
        {
            remainingBlockSpace = dp->rec_len - (dp->name_len + 8); // 8 is size of dir_entry without name
            if (remainingBlockSpace >= 8 + strlen(childName))       //There's room to insert dir_entry
            {
                dp->rec_len = dp->name_len + 8;
                cp += dp->rec_len;
                dp = (DIR_ENTRY *)cp;
                //Insert new dir entry here
                dp->file_type = 0; //FIXME: no idea
                dp->inode = childIno;
                dp->name_len = strlen(childName);
                strncpy(dp->name, childName, dp->name_len);
                dp->rec_len = remainingBlockSpace;
                put_block(dev, blk, buf);
                return 1;
            }
            else // No room, go to next block and allocate and make only entry
            {
                lblk++;            //Next block we want
                blk = balloc(dev); //allocate a block
                parent->INODE.i_block[lblk] = blk;
                get_block(dev, blk, buf); //Grab newly allocated block
                dp = buf;
                //Enter the file
                dp->file_type = 0; //FIXME: no idea
                dp->inode = childIno;
                dp->name_len = strlen(childName);
                strncpy(dp->name, childName, dp->name_len);
                dp->rec_len = BLKSIZE;    //Set to entire block because only entry
                put_block(dev, blk, buf); //Update the block in memory
                return 1;
            }
        }
        cp += dp->rec_len;    // advance cp by rec_len in BYTEs
        dp = (DIR_ENTRY *)cp; // pull dp along to the next record
        // }
    }
    //There was no room in current blocks, allocating new block
    // parent->INODE.i_block[totalBlocks] = balloc(dev);
    // parent->INODE.i_blocks++;
    // parent->INODE.i_size += BLKSIZE;
    // blk = parent->INODE.i_block[totalBlocks - 1]; //ONLY WORKS FOR SINGLE BLOCKS (not indirect)
    // get_block(dev, blk, buf);
    // //Put entry in
    // dp = (DIR_ENTRY *)buf;
    // dp->file_type = 0; //FIXME: no idea
    // dp->inode = childIno;
    // dp->name_len = strlen(childName);
    // strncpy(dp->name, childName, dp->name_len);
    // dp->rec_len = BLKSIZE; //Set to entire block because only entry
}

/*!
    Given an MINODE, returns it's parent MINODE.
    Returns NULL pointer if no parent is found (Means probably root or an error)
 */
MINODE *getParent(MINODE *mip)
{
    int ino, parentIno;
    MINODE *parent;
    ino = mip->ino;
    parentIno = search(&mip->INODE, "..");
    if (parentIno == 0)
    {
        printf("Failed to find .. in getParent\n");
        return 0;
    }
    parent = iget(dev, parentIno);
    return parent;
}

MINODE *getParentFromPath(char *childPath) {
    char tempPath[256], dBuf[256];
    int ino;

    strcpy(tempPath, childPath);
    if(childPath == 0) {
        printf("no path given\n");
        return 0;
    }
    strcpy(dBuf, dirname(tempPath));
    if(strcmp(dBuf, ".") == 0) {
        return running->cwd;
    }
    else {
        ino = getIno(dBuf);
        return iget(dev, ino);
    }
}

//Searches INODE ip for name. If found returns that Inode's inode number else 0
int search(INODE *ip, char *name)
{
    int i;
    char temp[128];
    char dbuf[BLKSIZE]; //Stores data block for ip
    DIR_ENTRY *dp;
    char *cp;

    for (i = 0; i < 12; i++) //At most 12 direct blocks
    {
        if (ip->i_block[i] == 0) //Break if data block is empty
        {
            if (TEST)
                printf("No data in block %d, breaking\n", i);
            break; //File is not in directory
        }

        get_block(dev, ip->i_block[i], dbuf);
        dp = (DIR_ENTRY *)dbuf;
        cp = dbuf;

        while (cp < dbuf + BLKSIZE) //While valid directory structure and not outside block
        {
            strcpy(temp, dp->name);
            temp[dp->name_len] = 0;
            if (TEST)
                printf("Comparing %s and %s\n", temp, name);
            if (strcmp(temp, name) == 0)
            {
                return dp->inode;
            }
            cp += dp->rec_len;    // advance cp by rec_len in BYTEs
            dp = (DIR_ENTRY *)cp; // pull dp along to the next record
        }
    }
    return 0;
}

int dir_empty(INODE *ip)
{
    int i;
    char temp[128];
    char dbuf[BLKSIZE]; //Stores data block for ip
    DIR_ENTRY *dp;
    char *cp;

    for (i = 0; i < 12; i++) //At most 12 direct blocks
    {
        if (ip->i_block[i] == 0) //Break if data block is empty
        {
            if (TEST)
                printf("No data in block %d, breaking\n", i);
            break; //File is not in directory
        }

        get_block(dev, ip->i_block[i], dbuf);
        dp = (DIR_ENTRY *)dbuf;
        cp = dbuf;

        while (cp < dbuf + BLKSIZE) //While valid directory structure and not outside block
        {
            strcpy(temp, dp->name);
            temp[dp->name_len] = 0;

            if (strcmp(temp, ".") != 0 && strcmp(temp, "..") != 0)
            {
                return 0;
            }
            cp += dp->rec_len;    // advance cp by rec_len in BYTEs
            dp = (DIR_ENTRY *)cp; // pull dp along to the next record
        }
    }
    return 1;
}
// THESE two functions are for pwd(running->cwd), which prints the absolute
// pathname of CWD.
//Copies the name of the ino from parent into myname
// DOES NOT SUPPORT INDIRECT BLOCKS
int findmyname(MINODE *parent, u32 myino, char *myname)
{
    INODE *inode;
    int i = 0;
    char buf[BLKSIZE];
    char *cp;
    DIR_ENTRY *dp;

    cp = buf;
    inode = &parent->INODE;
    while (inode->i_block[i] != 0)
    {
        //Load block into buf
        get_block(dev, inode->i_block[i], buf);
        while (cp < buf + BLKSIZE)
        {
            dp = (DIR_ENTRY *)cp;
            if ((u32)dp->inode == myino)
            {
                strncpy(myname, dp->name, dp->name_len);
                myname[dp->name_len] = '\0';
                return 1;
            }
            cp += dp->rec_len;
        }
    }
    return 0;
}

int check_EXT2(int fd)
{
    SUPER *sp;
    char buf[BLKSIZE];
    get_block(fd, 1, buf);
    sp = (SUPER *)buf;
    printf("s_magic = %x\n", sp->s_magic);
    if (sp->s_magic != 0xEF53)
    { //print magic number, check if a file system
        printf("NOT an EXT2 FS\n");
        return 0;
        ;
    }
    return 1;
}

int check_above(MINODE *wd, char *searchDir)
{ //recurses upwards to check if inside directory we are trying to mount to
    MINODE *nextMip;

    if (wd == root)
        return 0;
    nextMip = getParent(wd);
    if (search(&(nextMip->INODE), searchDir)) //check parent directories
        return 1;
    //Go up to next parent
    return check_above(nextMip, searchDir);
}

int rm_child(MINODE *parent, char *childName)
{
    //FIXME: Doesn't support indirect blocks, 
    //command rm: crashes here when trying to remove a file that wasnt the last file created
    //command rmdir: crashes here for some reason that has yet to be determined
    int fileSize, remainingBlockSpace, total_blocks, i, j;
    char buf[BLKSIZE], newBuf[BLKSIZE], temp[256], *cp;
    DIR_ENTRY *dp, *prevDp, *removedDp;

    parent->dirty = 1;
    total_blocks = getTotalBlocks(parent);

    for (i = 0; i < total_blocks; i++)
    {
        get_block(dev, parent->INODE.i_block[i], buf); //Get next block
        prevDp = 0;
        dp = (DIR_ENTRY *)buf;
        cp = buf;
        while (cp < buf + BLKSIZE) //While valid directory structure and not outside block
        {
            strncpy(temp, dp->name, dp->name_len);
            temp[dp->name_len] = 0;
            if (strcmp(temp, childName) == 0) //Found entry we want to remove
            {
                if (cp + dp->rec_len == buf + BLKSIZE) //This is the last entry
                {
                    if (prevDp == 0) //Only entry in block
                    {
                        //Deallocate block, move blocks upward in i_block
                        parent->INODE.i_size -= BLKSIZE;
                        parent->INODE.i_blocks--;
                        bdealloc(dev, parent->INODE.i_block[i]);
                        for (j = i; j < total_blocks - 1; j++) //loop through blocks and move block numbers
                        {
                            parent->INODE.i_block[j] = parent->INODE.i_block[j + 1];
                        }
                    }
                    else
                    {
                        prevDp->rec_len += dp->rec_len;                //Previous entry takes rest of block
                        put_block(dev, parent->INODE.i_block[i], buf); //Write modified block back
                    }
                }
                else //It's a middle entry or first with stuff after. same alg.
                {
                    removedDp = dp;
                    memcpy(newBuf, buf, BLKSIZE); //Copy the block buf
                    //Thought about for a while, pretty confident. Newbuf remaining data replaces removing record
                    memcpy(newBuf + (cp - buf), cp + dp->rec_len, buf + BLKSIZE - cp + dp->rec_len);
                    //Now search through new buf for the last rec to set rec_len correctly
                    //These variables don't matter anymore becuase we return immediately after
                    cp = newBuf;
                    dp = (DIR_ENTRY *)cp;
                    while (cp < newBuf + BLKSIZE)
                    {
                        if (cp + dp->rec_len + removedDp->rec_len == newBuf + BLKSIZE)
                        { //We found the last entry and now we set correct record length
                            dp->rec_len += removedDp->rec_len;
                            break;
                        }
                        cp += dp->rec_len;
                        dp = (DIR_ENTRY *)cp;
                    }
                    put_block(dev, parent->INODE.i_block[i], newBuf); //Write modified block back with the newBuf
                }
                return 1;
            }
            prevDp = dp;
            cp += dp->rec_len;    // advance cp by rec_len in BYTEs
            dp = (DIR_ENTRY *)cp; // pull dp along to the next record
        }
    }
    return 0;
}

int getTotalBlocks(MINODE *mip)
{
    int i = 0;
    while (mip->INODE.i_block[i] != 0)
        ++i;
    return i;
}

/*Algorithm to convert logical block to physical block
u32 map(INODE, lblk) {
    if (lblk < 12)
        blk = INODE.i_block[lblk];
    else if (12 <= lblk < 12+256) { //indirect blocks
            read INODE.i_block[12] into u32 ibuf[256];
            blk = ibuf[lblk - 12]
    }
    else {
        read INODE.i_block[13] into u32 dbuf[256];
        lblk -= (12+256);
        dblk = dbuf[lblk / 256];
        read dblk into dbuf[];
        blk = dbuf[lblk % 256];
    }
    return blk;
}
*/