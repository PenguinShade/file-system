#include "util.h"

int our_open(char *pathname, int flags) ;
int our_close(int fd);
int our_lseek(int fd, int position);
int our_read(int fd, char *buf, int nbytes);
int our_write(int fd, char *buf, int nbytes);
int zero_block(int fd, int block_num);