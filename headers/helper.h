#include "type.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <libgen.h>

#define TEST        0

#define FREE        0
#define READY       1

#define BLKSIZE  1024
#define MOUNT_CAPACITY 20 //Total number of mounts possible all at once
#define NMINODE    64
#define NFD        16
#define NMOUNT      4
#define NPROC       2
#define BUF_SIZE 256
#define DIR_PERM 0775   // drwxrwxr-x
#define FILE_PERM 0664  // -rw-rw-r--

#define SYMLNKTYPE 0120000
#define REGTYPE 0100000
#define DIRTYPE 0040000

//These are for our_open to open files in modes
#define OUR_R 0
#define OUR_W 1
#define OUR_RW 2
#define OUR_APPEND 3

SUPER *sp;
char sbuf[BLKSIZE];
GD *gp;
char gbuf[BLKSIZE];
INODE *ip;
MNT_ENTRY root_mnt_entry, *cur_mnt_entry;
// DIR_ENTRY *dp;

MINODE *root, minode[NMINODE];
PROC proc[NPROC], *running;

int dev, nblocks, ninodes, bmap, imap, inode_start,
    n, nameArrayLength;
char tempBuf[256], gpath[128], 
    *name[64],  
    ibuf[BLKSIZE]; //line[256];// cmd[32];// pathname[256];
    MNT_ENTRY mntable[MOUNT_CAPACITY]; 
int mntableSize;

int get_block(int fd, int blk, char buf[]);
int put_block(int fd, int blk, char buf[]);
int tokenize(char *line, char *names[]);
int tokenizePath(char *pathname, char *name[]);
MINODE *iget(int dev, int ino);
int iput(MINODE *mip); 
int getIno(char *pathname);
MINODE *getParent(MINODE *mip);
MINODE *getParentFromPath(char *childPath);
int search(INODE *ip, char *name);
int dir_empty(INODE *ip);
int findmyname(MINODE *parent, u32 myino, char *myname);
int check_EXT2(int fd);
int check_above(MINODE *wd, char *searchDir);
MINODE *insert_new_minode(MINODE *pmip, char *pathname, int mode, int gparentIno);
int enter_child(MINODE *parent, int childIno, char *childName);
int rm_child(MINODE *pmip, char *name);
int getTotalBlocks(MINODE *mip);