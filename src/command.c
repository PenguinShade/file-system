#include "command.h"

int our_ls(char *pathname)
{
    //FIXME: add link support
    int dev, ino;
    MINODE *mip;
    if (pathname == 0)
    {
        printf("using cwd\n");
        ls_dir(running->cwd);
    }
    else
    {
        printf("not cwd\n");
        dev = root->dev;
        ino = getIno(pathname); //Maybe add another argument dev
        if (ino == 0)
        {
            printf("Inode does not exist at path %s\n", pathname);
            return 0;
        }
        mip = iget(dev, ino);
        if (S_ISDIR(mip->INODE.i_mode))
        {
            ls_dir(mip);
        }
        else
        {
            printf("pathname: %s\n", pathname);
            ls_file(mip, basename(pathname));
        }
        iput(mip); //maybe mip may have been edited?
    }
    return 1;
}

char *our_pwd(MINODE *wd)
{
    if (wd == root)
        strcpy(tempBuf, "/");
    else
        rpwd(wd, 0, tempBuf);
    printf("%s\n", tempBuf);
    return tempBuf;
}

int our_cat(char *pathname)
{
    char *line = NULL, buf[BLKSIZE+1];
    int nread, fd;

    fd = our_open(pathname, OUR_R);

    if (fd == -1) //Failed to open
    {
        printf("failed to open file to read");
        return 0;
    }
    while (nread = our_read(fd, buf, BLKSIZE))
    {
        buf[nread] = 0;
        printf("%s\n", buf);
    }
    our_close(fd);
    return 1;
}

int our_cp(char *source, char *target)
{
    int fdread, fdwrite, n;
    char buf[BLKSIZE];

    if (!(source && target))
    {
        perror("Must have both paths\n");
        return 0;
    }
    if ((fdread = our_open(source, OUR_R)) < 0)
    {
        perror("Source file failed to open for read\n");
        return 0;
    }
    if ((fdwrite = our_open(target, OUR_W)) < 0)
    {
        perror("target file failed to open for write\n");
        return 0;
    }
    // printf("fdread=%d\n", fdread);
    // printf("fdwrite=%d\n", fdwrite);
    while (n = our_read(fdread, buf, BLKSIZE))
    {
        our_write(fdwrite, buf, n);
    }
    our_close(fdread);
    our_close(fdwrite);
    return 1;
}

int our_cd(char *pathname)
{
    int ino;
    MINODE *mip;
    printf("Entered our_cd\n");
    if (pathname == 0) //no pathname cd to root
    {
        if (TEST)
            printf("No pathname, cd to root\n");
        iput(running->cwd);
        //Grab root node of device
        mip = iget(dev, 2);
        running->cwd = mip;
    }
    else
    {
        //cd to pathname
        if (TEST)
            printf("Grabbing ino with getIno\n");
        //Handles starting with /
        ino = getIno(pathname);
        if (TEST)
            printf("inode number is %d\n", ino);
        if (ino == 0) //no inode found
            return 0;
        if (TEST)
            printf("Getting MINODE with ino: %d\n", ino);
        mip = iget(dev, ino);
        if (!S_ISDIR(mip->INODE.i_mode)) //If it's not a directory
        {
            printf("pathname is not a directory\n");
            iput(mip); //dispose grabbed MINODE
            return 0;
        }
        if (TEST)
            printf("Cleaning previous cwd and setting new cwd\n");
        iput(running->cwd);
        running->cwd = mip;
    }
    return 1;
}

int our_mkdir(char *pathname)
{
    char dirBuf[256], baseBuf[256], tempPath[256];
    int pino;
    MINODE *pmip, *mip;

    if(TEST)
        printf("strcpying\n");

    strcpy(tempPath, pathname);
    strcpy(dirBuf, dirname(tempPath));
    strcpy(tempPath, pathname);
    strcpy(baseBuf, basename(tempPath));

    if (TEST)
        printf("dirname: %s\tbasename: %s\n", dirBuf, baseBuf);

    pino = getIno(dirBuf);
    pmip = iget(dev, pino);

    if (!S_ISDIR(pmip->INODE.i_mode) || search(pmip, baseBuf))
    {
        printf("dirname is not a dir\nbasename already exists in parent DIR");
        return 0;
    }

    mip = insert_new_minode(pmip, pathname, DIRTYPE | DIR_PERM, pmip->ino);
    mip->refCount += 2;

    insert_new_minode(mip, ".", DIRTYPE | DIR_PERM, pmip->ino);
    mip->INODE.i_size = BLKSIZE;
    //iput(mip);

    insert_new_minode(mip, "..", DIRTYPE | DIR_PERM, pmip->ino);
    iput(mip);

    pmip->INODE.i_links_count += 1;
    iput(pmip);
    return 1;

    /*
    int mkdir(char *pathname)
    {
        1. if (pathname is absolute) dev = root->dev;
            else dev = PROC's cwd->dev 
        2. divide pathname into dirname and basename; 
        3. // dirname must exist and is a DIR:
            pino = getino(&dev, dirname);
            pmip = iget(dev, pino);
            check pmip      ->INODE is a DIR 
        4. // basename must not exist in parent DIR:
            search(pmip, basename) must return 0;
        5. call kmkdir(pmip, basename) to create a DIR;
            (pseudocode in helper.c)
        6. increment parent INODE's links_count by 1 and mark pmip dirty; return 1;
            iput(pmip); 
    } */
}

int our_rmdir(char *pathname)
{
    int ino, pino, i;
    MINODE *mip, *pmip;
    char base[256], tempPath[256];

    strcpy(tempPath, pathname);
    ino = getIno(pathname);
    mip = iget(dev, ino);
    if (!S_ISDIR(mip->INODE.i_mode))// || mip->refCount > 0)
    {
        printf("not a dir\n");
        return 0;
    }
    //check if contains only . and ..
    if (!dir_empty(&(mip->INODE)))
    {
        printf("dir not empty\n");
        return 0;
    }

    pmip = getParent(mip);
    strcpy(base, basename(tempPath));
    if (!findmyname(pmip, ino, base))
    {
        printf("could not find directory to remove\n");
        return 0;
    }
    our_truncate(mip);
    idealloc(mip->dev, ino);
    iput(mip);
    rm_child(pmip, base);

    pmip->dirty = 1;
    iput(pmip);

    /*  touch pip's atime, mtime fields;
    mark pip dirty;
    iput(pip);
     return SUCCESS; */

    /* 1. get in - memory INODE of pathname:
            ino = getino(&de, pathanme);
            mip = iget(dev,ino);
        2. verify INODE is a DIR (by INODE.i_mode field);
            minode is not BUSY (refCount = 1);
            DIR is empty (traverse data blocks for number of entries = 2);
        3. /* get parent's ino and inode 
            pino = findino(); //get pino from .. entry in INODE.i_block[0]
            pmip = iget(mip->dev, pino);
        4. /* remove name from parent directory 
            findname(pmip, ino, name); //find name from parent DIR
            rm_child(pmip, name);
        5. /* deallocate its data blocks and inode 
            truncat(mip); // deallocate INODE's data blocks
        6. deallocate INODE
            idalloc(mip->dev, mip->ino); iput(mip);
        7. dec parent links_count by 1;
            mark parent dirty; iput(pmip);
        8. return 0 for SUCCESS. */
    return 1;
}

int our_creat(char *pathname)
{
    char dirBuf[256], baseBuf[256], tempPath[256];
    int pino;
    MINODE *pmip, *mip;

    strcpy(tempPath, pathname);
    strcpy(dirBuf, dirname(tempPath));
    strcpy(tempPath, pathname);
    strcpy(baseBuf, basename(tempPath));

    if (TEST)
        printf("dirname: %s\tbasename: %s\n", dirBuf, baseBuf);

    pino = getIno(dirBuf);
    pmip = iget(dev, pino);

    if (!S_ISDIR(pmip->INODE.i_mode) || search(pmip, baseBuf))
    {
        printf("dirname is not a reg\nbasename already exists in parent DIR");
        return 0;
    }
    mip = insert_new_minode(pmip, pathname, REGTYPE | FILE_PERM, pmip->ino);
    mip->INODE.i_mode |= FILE_PERM;
    iput(pmip);
    return 1;

    /*
    This is similar to mkdir() except
    1. the INODE.i_mode fielld is set to REG file type, permission
        bits set to 0644 for rw-r--r--, and
    2. no data block is allocated for it, so the file size is 0
    3. do not increment parent INODE's links-count
    */
}

int our_rm(char *pathname)
{
    if(our_unlink(pathname))
        return 1;
    return 0;
}

int our_touch(char *pathname)
{
    if(TEST)
        printf("inside our_touch\n");
    
    time_t mytime = time(NULL);

    int ino, touchedIno;
    MINODE *mip, *pmip;
    char tempPath[256], dBuf[256], bBuf[256];

    strcpy(tempPath, pathname);
    strcpy(dBuf, dirname(tempPath));
    strcpy(tempPath, pathname);
    strcpy(bBuf, __xpg_basename(tempPath));

    ino = getIno(dBuf);
    touchedIno = getIno(pathname);
    mip = iget(dev, touchedIno);
    pmip = iget(dev, ino);
    if(search(&(pmip->INODE), bBuf)){ //files exists so just update times
        if(TEST)
            printf("found existing file, updating times\n");
        mip->INODE.i_ctime = mytime;
    }
    else{ //file doesnt exists so create it
        insert_new_minode(pmip, pathname, REGTYPE | FILE_PERM, pmip->ino);
        iput(pmip);
    }
    return 1;
}

int our_chmod(char *mode, char *pathname)
{
    //FIXME:
    int ino, intMode = 0, temp, i, mask;
    MINODE *mip;
    mask = 1;
    if(strlen(mode) == 3) {
        for(i = 0; i < strlen(mode); i++) {
            temp = mode[i] - 48;
            temp = temp << (2 - i)*3;
            intMode |= temp;
        }

        ino = getIno(pathname);
        if (ino == 0)
        {
            printf("Invalid pathname\n");
            return 0;
        }
        mip = iget(dev, ino);
        mip->INODE.i_mode &= ~0777;
        mip->INODE.i_mode |= intMode;
        mip->dirty = 1;
        iput(mip);
        /* 2. chmod filename mode: (mode = |rwx|rwx|rwx|, e.g. 0644 in octal)
            get INODE of pathname into memroy:
                ino = getino(pathname);
                mip = iget(dev, ino);
                mip->INODE.i_mode |= mode;
            mip->dirty = 1;
            iput(mip); */
        return 1;
    }
}

int our_open_file(char *pathname, char *mode_str)
{
    int mode;
    
    if (strcmp(mode_str, "R") == 0)
        mode = OUR_R;
    else if (strcmp(mode_str, "W") == 0)
        mode = OUR_W;
    else if (strcmp(mode_str, "RW") == 0)
        mode = OUR_RW;
    else if (strcmp(mode_str, "A") == 0)
        mode = OUR_APPEND;
    return our_open(pathname, mode);
}

int our_close_file(char *fd_str)
{
    return our_close(atoi(fd_str));
}

int print_fd()
{
    int i = 0;
    OFT *curFt;
    printf("============================================\n");
    printf("fd  mode  count  offset  [dev,ino]  filename\n");
    printf("--  ----  -----  ------  ---------  --------\n");
    for (i = 0; i < NFD; i++)
    {
        curFt = &running->fd[i];
        if (curFt->refCount != 0)
        {
            printf("%2d  ", i); //print fd
            switch (curFt->mode)
            {
                case OUR_R:
                    printf("READ");
                break;
                case OUR_W:
                    printf("WRTE");
                break;
                case OUR_RW:
                    printf("RDWR");
                break;
                case OUR_APPEND:
                    printf("APPD");
                break;
                default:
                    printf("INVD");
            }
            printf("  %5d  ", curFt->refCount);
            printf("%6d  ", curFt->offset);
            printf("[%2d,%4d]  ", curFt->mptr->dev, curFt->mptr->ino);
            printf("Q\n"); //FIXME: Somehow keep track of filename if an oft by adding variable probably
        }
    }
    printf("============================================\n");
    return 1;
}

int our_read_file(char *fd_str, char *nbytes_str)
{
    int success = 0, fd, nbytes, n;
    char buf[8192]; 

    fd = atoi(fd_str);
    nbytes = atoi(nbytes_str);
    n = our_read(fd, buf, nbytes);
    buf[n] = 0;
    printf("%s\n", buf);
    return n;
}

int our_write_file(char *fd_str, char *in_buf)
{
    int fd, firstNullFound = 0;
    char *inputStart;

    inputStart = in_buf+1;
    if (in_buf[0] != '"')
    {
        printf("Input must start with \"\n");
        return -1;
    }
    fd = atoi(fd_str);
    if (running->fd[fd].refCount == 0)
    {
        printf("Fd is not opened\n");
        return -1;
    }
    if (running->fd[fd].mode == OUR_R)
    {
        printf("Fd is only opened for read, cannot write\n");
        return -1;
    }

    //Get full input
    in_buf += 1;
    while(*in_buf != '"')
    {
        if (!firstNullFound && *in_buf == 0)
        {
            firstNullFound = 1;
            *in_buf = ' ';
            in_buf++;
            continue;
        }
        in_buf++;
    }
    *in_buf = 0; //Set null termination at last "
    return our_write(fd, inputStart, strlen(inputStart));
}

int our_lseek_fd(char *fd, char *position)
{
    return our_lseek(atoi(fd), atoi(position));
}

int our_mv(char *src, char *dst)
{
    //TODO: make case if devs are different, use cp
    our_link(src, dst);
    our_unlink(src);
}

int our_mount(char *fs, char *pathname)
{ //fs is the filesystem to be mounted to pathname
    int i = 0, fd;
    MNT_ENTRY *m;
    MINODE *mountPt;

    for (i = 0; i < mntableSize; i++)
    {
        if (strcmp(mntable[i].name, fs) == 0)
        {
            printf("Already mounted\n");
            return 0;
        }
    }
    fd = open(fs, O_RDWR);
    if (!check_EXT2(fd) && !check_above(running->cwd, pathname))
        return 0;
    //init new MNT_ENTRY
    m = &mntable[mntableSize];
    m->fd = fd;
    strcpy(m->name, pathname);
    mountPt = iget(dev, getIno(pathname));
    if (!S_ISDIR(mountPt->INODE.i_mode))
    {
        printf("Cannot mount to non-directory\n");
        return 0;
    }
    m->mntPoint = mountPt;
    mountPt->mounted = 1;
    mountPt->mptr = m;

    load_mnt_entry(m);

    /* 
1. If no parameter, display current mounted file systems;
2. Check whether filesys is already mounted:
    The MOUNT table entries contain mounted file system (device) names
    and their mounting points. Reject if the device is already mounted.
    If not, allocate a free MOUNT table entry.
3. filesys is a special file with a device number dev=(major,minor).
    Read filesys' superblock to verify it is an EXT2 FS.
4. find the ino, and then the minode of mount_point:
    call ino = get_ino(&dev, pathname); to get ino:
    call mip = iget(dev, ino); to load its inode into memory;
5. Check mount_point is a DIR and not busy, e.g. not someone's CWD.
6. Record dev and filesys name in the MOUNT table entry;
    also, store its ninodes, nblocks, etc. for quick reference.
7. Mark mount_point's minode as mounted on (mounted flag = 1) and let
    it point at the MOUNT table entry, which points back to the
    mount_point minode. 
*/
}

int our_umount(char *fs)
{
    //TODO:
    /*  1. Search the MOUNT table to check filesys is indeed mounted.
    2. Check (by checking all active minode[].dev) whether any file is
        active in the mounted filesys; If so, reject;
    3. Find the mount_point's in-memory inode, which should be in memory
        while it's mounted on. Reset the minode's mounted flag to 0; then
        iput() the minode.
    4. return SUCCESS; */
}