#include "helper.h"

int tst_bit(char *buf, int bit);
int set_bit(char *buf, int bit);
int clr_bit(char *buf, int bit);
int decFreeBlocks(int dev);
int incFreeBlocks(int dev) ;
int decFreeInodes(int dev);
int incFreeInodes(int dev) ;
int balloc(int dev);
int bdealloc(int fd, int blk);
int ialloc(int dev);
int idealloc(int dev, int ino);
int our_truncate(MINODE *mip);