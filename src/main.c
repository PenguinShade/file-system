#include "command.h"
//#include "testFunctions.c"

int init()
{
    int i, j;
    MINODE *mip;
    PROC *p;

    printf("init()\n");
    mntableSize = 0; //initialize size of mntable
    for (i = 0; i < NMINODE; i++)
    {
        mip = &minode[i];
        // set all entries to 0;
        mip->dev = 0;
        mip->dirty = 0;
        mip->ino = 0;
        mip->refCount = 0;
        mip->mounted = 0;
        mip->mptr = 0;
    }
    for (i = 0; i < NPROC; i++)
    {
        p = &proc[i];
        // set pid = i; uid = i; cwd = 0;
        p->pid = i;
        p->uid = i;
        p->cwd = 0;
        p->status = FREE; //0
        for (j = 0; j < NFD; j++)
        {
            p->fd[j].refCount = 0; //Refcount how many places this files opened
        }
    }
}

int quit()
{
    int i;
    MINODE *mip;
    for (i = 0; i < NMINODE; i++)
    {
        mip = &minode[i];
        if (mip->refCount > 0)
            iput(mip);
    }
    exit(0);
}

int main(int argc, char *argv[])
{
    int ino, fd;
    char buf[BLKSIZE], line[1024], *cmd = 0, *pathname1 = 0, *pathname2 = 0;
    char *disk = "mydisk";
    if (argc > 1)
        disk = argv[1];
    fd = open(disk, O_RDWR);
    printf("fd: %d\n", fd);
    if (fd < 0)
    {
        printf("open %s failed\n", disk);
        exit(1);
    }
    dev = fd;

    init();
    mount_root();
    printf("root refCount = %d\n", root->refCount);

    printf("creating P0 as running process\n");
    running = &proc[0];
    running->status = READY;
    running->cwd = iget(dev, 2);
    // set proc[1]'s cwd to root also
    printf("root refCount = %d\n", root->refCount);

    // if (TEST)
    //     run_tests();

    while (1)
    {
        printf(" _______________________________________________________\n");
        printf("| pwd\tls\tcd\tcat\tmkdir\trmdir\t\t|\n");
        printf("| creat\trm\ttouch\tclose\tunlink\tsymlink\t\t|\n");
        printf("| mount\tumount\tchmod\topen\tlseek\tmv\tlink\t|\n");
        printf("|_______________________________________________________|\n\n");
        printf("input command : ");
        fgets(line, 128, stdin);
        line[strlen(line) - 1] = 0;

        if (line[0] == 0)
            continue;

        cmd = strtok(line, " ");
        pathname1 = strtok(NULL, " ");
        pathname2 = strtok(NULL, " ");

        // sscanf(line, "%s %s", cmd, pathname);
        printf("cmd=%s pathname=%s\n", cmd, pathname1);

        //************Commands that require no path*****************
        if (strcmp(cmd, "pwd") == 0)
            our_pwd(running->cwd);
        else if (strcmp(cmd, "ls") == 0)
            our_ls(pathname1);
        else if (strcmp(cmd, "quit") == 0)
            quit();
        else if (strcmp(cmd, "pfd") == 0)
            print_fd();
        else if (pathname1 == 0) //Check for a pathname
        {
            printf("Invalid, need a path\n");
            continue;
        }
        //************Commands require one path*****************
        else if (strcmp(cmd, "cd") == 0)
            our_cd(pathname1);
        else if (strcmp(cmd, "cat") == 0)
            our_cat(pathname1);
        else if (strcmp(cmd, "mkdir") == 0)
            our_mkdir(pathname1);
        else if (strcmp(cmd, "rmdir") == 0)
            our_rmdir(pathname1);
        else if (strcmp(cmd, "creat") == 0)
            our_creat(pathname1);
        else if (strcmp(cmd, "rm") == 0)
            our_rm(pathname1);
        else if (strcmp(cmd, "touch") == 0)
            our_touch(pathname1);
        else if (strcmp(cmd, "close") == 0)
            our_close_file(pathname1);
        else if (strcmp(cmd, "unlink") == 0)
            our_unlink(pathname1);
        else if (strcmp(cmd, "umount") == 0)
            our_umount(pathname1);
        else if (strcmp(cmd, "stat") == 0) 
            our_stat(pathname1);
        else if (pathname2 == 0) //Check for a pathname
        {
            printf("Invalid, need a second path\n");
            continue;
        }
        //***********Commands require two paths****************
        else if (strcmp(cmd, "cp") == 0)
            our_cp(pathname1, pathname2);
        else if (strcmp(cmd, "link") == 0)
            our_link(pathname1, pathname2);
        else if (strcmp(cmd, "symlink") == 0)
            our_symlink(pathname1, pathname2);
        else if (strcmp(cmd, "chmod") == 0)
            our_chmod(pathname1, pathname2);
        else if (strcmp(cmd, "open") == 0)
            our_open_file(pathname1, pathname2);
        else if (strcmp(cmd, "lseek") == 0)
            our_lseek_fd(pathname1, pathname2);
        else if (strcmp(cmd, "mv") == 0)
            our_mv(pathname1, pathname2);
        else if (strcmp(cmd, "mount") == 0)
            our_mount(pathname1, pathname2);
        else if (strcmp(cmd, "write") == 0)
            our_write_file(pathname1, pathname2);
        else if (strcmp(cmd, "read") == 0)
            our_read_file(pathname1, pathname2);
    }
    return 1;
}