#include <ext2fs/ext2_fs.h>

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;

typedef struct ext2_super_block SUPER;
typedef struct ext2_group_desc GD;
typedef struct ext2_inode INODE;
typedef struct ext2_dir_entry_2 DIR_ENTRY;

typedef struct minode
{
    INODE INODE;
    int dev, ino;
    int refCount;
    int dirty;
    int mounted;
    struct mount_entry *mptr;
} MINODE;

typedef struct oft
{
    int mode;
    int refCount;
    MINODE *mptr;
    int offset;
} OFT;

typedef struct proc
{
    struct proc *next;
    int pid;
    int ppid;
    int status;
    int uid, gid;
    MINODE *cwd;
    OFT fd[16];
} PROC;

typedef struct mount_entry
{
    int fd;
    char name[1024];
    MINODE *mntPoint;
} MNT_ENTRY;