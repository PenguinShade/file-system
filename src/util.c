#include "util.h"

int our_link(char *oldFile, char *newFile)
{
    int oino, nino;
    MINODE *omip, *pmip;
    oino = getIno(oldFile);
    if (oino == 0)
    {
        printf("source file does not exist\n");
        return 0;
    }
    omip = iget(dev, oino); //Gets old file MINODE

    if (S_ISDIR(omip->INODE.i_mode))
    {
        printf("cannot link a dir\n");
        return 0;
    }
    if (getIno(newFile) > 0)
    {
        printf("file already exists\n");
        return 0;
    }

    pmip = getParentFromPath(newFile); //Get parent from new file
    enter_child(pmip, oino, newFile);
    omip->dirty = 1;
    omip->INODE.i_links_count += 1;
    iput(omip);
    iput(pmip);
    return 1;
    /*  1. // verify old_file exists and is not DIR;
            oino = getino(&odev, old_file);
            omip = iget(odev, oino);
            check file type (cannot be DIR).
        2. // new_file must not exist yet:
            nion = get(&ndev, new_file) must return 0;
            ndev of dirname(newfile) must be same as odev
        3. // creat entry in new_parent DIR with same ino
            pmip -> minode of dirname(new_file);
            enter_child(pmip, omip->ino, basename(new_file));
        4. omip->INODE.i_links_count++;
            omip->dirty = 1;
            iput(omip);
            iput(pmip); */
}

int our_unlink(char *pathname)
{
    int ino;
    MINODE *mip, *pmip;
    char tempPath[256];

    strcpy(tempPath, pathname);
    ino = getIno(pathname);
    if (ino == 0)
    {
        printf("file does not exist\n");
        return 0;
    }
    mip = iget(dev, ino);
    if(S_ISDIR(mip->INODE.i_mode)) {
        printf("this is a dir\n");
        return 0;
    }
    pmip = getParentFromPath(pathname);

    rm_child(pmip, __xpg_basename(tempPath));
    pmip->dirty = 1;
    iput(pmip);

    mip->INODE.i_links_count--;
    if (mip->INODE.i_links_count > 0)
    {
        mip->dirty = 1;
        iput(mip);
    }
    if (!S_ISLNK(mip->INODE.i_mode))
    { //no link, so truncate all data
        our_truncate(mip);
        idealloc(dev, ino);
    }

    /*  1. get filenmae's minode:
            ino = getino(&dev, filename);
            mip = iget(dev, ino);
            check it's a REG or SLINK file
        2. // remove basename from parent DIR
            rm_child(pmip, mip->ino, basename);
            pmip->dirty = 1;
            iput(pmip);
        3. // decrement INODE's link_count
            mip->INODE.i_links_count--;
            if (mip->INODE.i_links_count > 0){
                mip->dirty = 1; iput(mip);
            }
        4. if (!SLINK file)
            // assume:SLINK file has no data block
            truncate(mip); // deallocate all data blocks
            deallocate I */
    return 1;
}

int our_symlink(char *oldFile, char *newFile)
{
    int oino, nino, pnino;
    MINODE *omip, *nmip, *pnmip;
    char newDirname[256], buf[BLKSIZE];

    oino = getIno(oldFile);
    nino = getIno(newFile);
    if (oino == 0 || nino > 0)
    {
        printf("link these two files\n");
        return 0;
    }

    //create newFile
    strcpy(newDirname, dirname(newFile));
    pnino = getIno(newDirname);
    pnmip = iget(dev, pnino);                             //parent MINODE of newFile
    nmip = insert_new_minode(pnmip, newFile, SYMLNKTYPE | FILE_PERM, pnmip->ino); //create new MINODE under pnmip;
    nmip->INODE.i_mode = (0120000) | 0777;                       //set newFile MINODE to LNK type
    nmip->INODE.i_size = strlen(oldFile);
    nmip->INODE.i_block[0] = balloc(dev);

    get_block(dev, nmip->INODE.i_block[0], buf);
    strcpy(buf, oldFile);
    put_block(dev, nmip->INODE.i_block[0], buf);
    nmip->dirty = 1;
    iput(nmip);
    iput(pnmip);
    return 1;

    /*      1. check: old_file must exist and new_file not yet exist;
        2. create new_file; change new_file to SLINK type;
        3. // assume length of old_file name <= 60 chars
            store old_file name in newfile's INODE.i_block[ ] area.
            mark new_file's minode dirty;
            iput(new_file's minode);*
        4. mark new_file parent minode di 
            put(new_file's parent minode);*/
}

int our_stat(char *pathname) //, struct stat *buf)
{
    //FIXME:
    MINODE *mip;
    struct stat buf;
    int ino;
    char t[256];

    ino = getIno(pathname);
    mip = iget(dev, ino);

    if (ino == 0 || mip == 0)
    {
        printf("failed to open pathname ino\n");
        return 0;
    }

    printf("size: %d\t", mip->INODE.i_size);
    printf("blocks: %d\t",mip->INODE.i_blocks);
    printf("blksize: %d\t",BLKSIZE);
    printf("filetype: %d\n", mip->INODE.i_mode);
    printf("dev: %d\t", mip->dev);
    printf("ino: %d\t", mip->ino);
    printf("lnks: %d\t", mip->INODE.i_links_count);
    printf("uid: %d\t", mip->INODE.i_uid);
    printf("gid: %d\n", mip->INODE.i_gid);
    memcpy(t, &((char *)ctime((const time_t *)&mip->INODE.i_atime))[4], 12);
    t[12] = 0;
    printf("access: %s\n", t);
    memcpy(t, &((char *)ctime((const time_t *)&mip->INODE.i_mtime))[4], 12);
    t[12] = 0;
    printf("modify: %s\n", t);
    memcpy(t, &((char *)ctime((const time_t *)&mip->INODE.i_ctime))[4], 12);
    t[12] = 0;
    printf("change: %s\n", t);

    /*  (1). get INODE of pathname into a minode;
        (2). copy (dev, ino) of minode to (st_dev, st_ino) of the STAT  structure in user space;
        (3). copy other fields of INODE to STAT structure in user space;
        (4). iput(minode); retrun 1 for OK; 
        
        struct stat myst;
        get INODE of filename into memory:
            int ino = getino(pathname);
            MINODE *mip = iget(dev, ino);
            copy dev, ino to myst.st_dev, myst.st_ino;
            copy mip->INODE fields to myst fields;
        iput(mip);*/
    //stat(pathname, buf);
    return 1;
}

int ls_file(MINODE *mip, char *name)
{
    int i;
    char t[256];
    u16 mode, mask;

    //printf("ls_file\n");
    if (mip != 0)
    {
        mode = mip->INODE.i_mode;
        if (S_ISDIR(mode))
            putchar('d');
        else if (S_ISLNK(mode))
            putchar('l');
        else
            putchar('-');
        mask = 000400;
        for (i = 0; i < 3; ++i)
        {
            if (mode & mask)
                putchar('r');
            else
                putchar('-');
            mask = mask >> 1;
            if (mode & mask)
                putchar('w');
            else
                putchar('-');
            mask = mask >> 1;
            if (mode & mask)
                putchar('x');
            else
                putchar('-');
            mask = mask >> 1;
        }
        printf(" %d", mip->INODE.i_links_count);
        printf(" %d", mip->INODE.i_uid);
        printf(" %d", mip->INODE.i_gid);
        memcpy(t, &((char *)ctime((const time_t *)&mip->INODE.i_ctime))[4], 12);
        t[12] = 0;
        printf(" %s", t);
        printf(" %6d", mip->INODE.i_size);
        printf(" %s\n", name);
    }
}

int ls_dir(MINODE *mip)
{
    int ino, i;
    char buf[BLKSIZE];
    char temp[1024];
    MINODE *dip;
    DIR_ENTRY *dp;
    char *cp;
    printf("ls_dir\n");
    // ino = getino(dirname);
    // mip = iget(dev, ino);
    for (i = 0; i < 12; i++)
    {
        if (mip->INODE.i_block[i] == 0)
            break;
        printf("Searching block[%d]\n", i);
        get_block(mip->dev, mip->INODE.i_block[i], buf);
        dp = (DIR_ENTRY *)buf;
        cp = buf;
        //Search through each record entry
        while (cp < buf + BLKSIZE)
        {
            strncpy(temp, dp->name, dp->name_len);
            temp[dp->name_len] = 0;
            // printf("%s ", temp);
            dip = iget(mip->dev, dp->inode);
            ls_file(dip, temp);
            cp += dp->rec_len;
            dp = (DIR_ENTRY *)cp;
        }
    }
}
/*!
    Helper function for pwd
 */
int rpwd(MINODE *wd, int childIno, char *buf)
{
    MINODE *nextMip;
    int parentIno;
    char myname[256];

    if (wd == root)
    {
        strcpy(buf, "\0");
    }
    else
    {
        //Go up to next parent
        nextMip = getParent(wd);
        rpwd(nextMip, wd->ino, buf);
    }
    //Add directory name to string
    if (childIno > 1) //valid ino
    {
        findmyname(wd, childIno, myname);
        // if (nextMip == root)
        strcat(buf, "/");
        strcat(buf, myname);
    }
    //Write back (nothing to write) and then dispose from buf
    iput(wd);
    return 1;
}

int mount_root()
{
    char buf[BLKSIZE];
    printf("mount_root\n");
    root_mnt_entry.fd = dev;
    strcpy(root_mnt_entry.name, ""); //should never need to use
    root_mnt_entry.mntPoint = NULL;  //This is the root yah....
    load_mnt_entry(&root_mnt_entry);
}

/*!
    mnt points to a mount in the table. Updates globals
    and sets where it is mounted from (mntpointer)
 */
int load_mnt_entry(MNT_ENTRY *mnt)
{
    char buf[BLKSIZE];

    mnt->mntPoint = root;
    dev = mnt->fd; //dev is an fd
    if (!check_EXT2(dev))
        return 0;
    printf("EXT2 FS OKAY\n");
    get_block(dev, 1, sbuf);
    sp = (SUPER *)sbuf;
    nblocks = sp->s_blocks_count;
    ninodes = sp->s_inodes_count;
    /* (2). get GD0 in Block #2:
       record bmap, imap, inodes_start as globals*/
    get_block(dev, 2, gbuf);
    gp = (GD *)gbuf;
    bmap = gp->bg_block_bitmap;
    imap = gp->bg_inode_bitmap;
    inode_start = gp->bg_inode_table;
    root = iget(dev, 2); //root is the second inode number
    if (root == 0)
    {
        printf("mounted root failed\n");
        exit(1);
    }
    cur_mnt_entry = mnt;
    return 1;
}

// int mount_to_minode(MNT_ENTRY *mnt)
// {

// }