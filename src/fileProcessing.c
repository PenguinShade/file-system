#include "fileProcessing.h"

/*!
    Opens a file for read, write, read write or append. Will return the file descriptor number.
    Returns -1 if failed to open file.
 */
int our_open(char *pathname, int flag)
{
    int ino, i;
    MINODE *mip;
    OFT *ft;

    ino = getIno(pathname);
    if (ino == 0 && O_CREAT)
    {
        our_creat(pathname);
        ino = getIno(pathname);
    }
    mip = iget(dev, ino);
    //TODO: check file permissions
    //First check to see if already opened and return if so and in read mode
    for (i = 0; i < NFD; i++)
    {
        ft = &running->fd[i];
        if (ft->refCount > 0 && ft->mode == OUR_R && ft->mptr == mip)
        {
            ft->refCount++;
            return i; //already active, return descriptor
        }
    }
    //Not already opened check for empty fd
    for (i = 0; i < NFD; i++)
    {
        ft = &running->fd[i];
        if (ft->refCount == 0) //Found empty file descriptor
        {
            //initialize this file descriptor because first empty entry.
            ft->mode = flag;
            ft->offset = 0;
            ft->mptr = mip;
            ft->refCount++;
            switch (flag)
            {
            case OUR_R:
                ft->offset = 0; // R: offset = 0
                break;
            case OUR_W:
                //FIXME: Make new file if doesn't exist? Actually it seems he doesn't care according to cp help
                our_truncate(mip); // W: truncate file to 0 size
                ft->offset = 0;
                break;
            case OUR_RW:
                ft->offset = 0; // RW: do NOT truncate file
                break;
            case OUR_APPEND:
                ft->offset = mip->INODE.i_size; // APPEND mode
                break;
            default:
                printf("invalid mode\n");
                return (-1);
            }
            return i;
        }
    }
    return -1; //Failed to open, full
}

int our_close(int fd)
{
    MINODE *mip;
    OFT *ft = &running->fd[fd];
    mip = ft->mptr;

    if (fd >= NFD)
    {
        printf("fd out of range, invalid\n");
        return 0;
    }
    // if (mip->refCount == 0) //valid file descriptor
    if (ft->refCount == 0)
    {
        printf("fd not opened\n");
        return 0;
    }
    // if (mip->refCount-- == 0) //Decrement refcount because close
    if (--ft->refCount == 0)
    {
        iput(mip); //Deactivating fd, save changes that were made
    }
    return 1;
}

int our_lseek(int fd, int position)
{
    OFT *ft = &running->fd[fd];
    int size, prevPosition;

    if (ft->refCount == 0)
    {
        printf("Invalid fd for lseeking\n");
        return 0;
    }
    size = ft->mptr->INODE.i_size;
    if (position < 0 || position >= size)
    {
        printf("Invalid position\n");
        return 0;
    }
    prevPosition = ft->offset;
    ft->offset = position;
    return prevPosition; //Return the previous position
}

int our_read(int fd, char *out_buf, int nbytes)
{
    int count = 0, available, blk, lblk, startByte, i, remainBytes, total_read_bytes,
        cpyBytes, offset;
    char buf[BLKSIZE], readbuf[BLKSIZE];
    int *ip;
    OFT *ft;
    MINODE *mip;

    //FIXME: How do I lock mip?
    ft = &running->fd[fd];
    offset = ft->offset;
    ip = (int *)buf;
    mip = ft->mptr;
    total_read_bytes = nbytes;

    available = mip->INODE.i_size - ft->offset;

    while (nbytes && available)
    {
        //Compute logical block and start byte
        lblk = offset / BLKSIZE;
        startByte = offset % BLKSIZE;
        if (lblk < 12)
        {
            blk = mip->INODE.i_block[lblk];
        }
        else if (lblk >= 12 && lblk < (256 + 12))
        {
            get_block(dev, mip->INODE.i_block[12], buf);
            lblk = lblk - 12;
            blk = *(ip + lblk);
        }
        else //We assume double indirect (not implementing triple)
        {
            get_block(dev, mip->INODE.i_block[13], buf);
            lblk = lblk - 256 - 12;
            blk = *(ip + lblk / 256); //Get indirect block
            lblk %= 256;              //Offset in the indirect block
            get_block(dev, blk, buf);
            blk = *(ip + lblk);
        }

        //Grabbed the blk now read data from the grabbed block
        get_block(dev, blk, readbuf);
        remainBytes = BLKSIZE - startByte;
        if (nbytes > remainBytes) //has more to read then what the rest of this block offers
        {
            if (available < remainBytes) //available memory is less than what is wanted to read
                remainBytes = available;
            memcpy(out_buf, readbuf + startByte, remainBytes);
            nbytes -= remainBytes;
            out_buf += remainBytes; //Increment to next location for next
            available -= remainBytes;
            offset += remainBytes;
        }
        else //This block is sufficient to get remaining data in read
        {
            cpyBytes = nbytes;
            if (available < nbytes) //will read into unallocated data if past available
                cpyBytes = available;
            memcpy(out_buf, readbuf + startByte, cpyBytes);
            nbytes -= cpyBytes;
            available -= cpyBytes;
            offset += cpyBytes;
            break; //Done looping (don't need this break but clearer)
        }
    }
    ft->offset = offset;              //Set new offset
    return total_read_bytes - nbytes; //Return how many bytes were read
}

/*!
    Writes nbytes to file corresponding to the file descriptor fd.
    Returns numbers of bytes actually written.
 */
int our_write(int fd, char *in_buf, int nbytes)
{
    int count = 0, blk, lblk, startByte, i, remainBytes, total_read_bytes,
      cpyBytes, offset, fileSize, oldBlk;
    char buf[BLKSIZE], readbuf[BLKSIZE], kbuf[BLKSIZE], *ubuf, *cp;
    int *ip;
    OFT *ft;
    MINODE *mip;

    //FIXME: Lock the file somehow
    ft = &running->fd[fd];
    mip = ft->mptr;
    cp = in_buf; //at first set to beginning of in_buf
    offset = ft->offset;
    mip->dirty = 1; 
    ip = (int *)buf;
    fileSize = mip->INODE.i_size;
    total_read_bytes = nbytes;

    while (nbytes)
    {
        //Compute logical block and start byte
        lblk = offset / BLKSIZE;
        startByte = offset % BLKSIZE;
        //**********This is to grab the block from memory********* */
        if (lblk < 12)
        {
            if (mip->INODE.i_block[lblk] == 0)
            { // if no data block yet
                mip->INODE.i_block[lblk] = balloc(mip->dev); // MUST ALLOCATE a block
                iput(mip); //Store block update
                zero_block(mip->dev, mip->INODE.i_block[lblk]);
            }
            blk = mip->INODE.i_block[lblk];
        }
        else if (lblk >= 12 && lblk < (256 + 12)) //Indirect block
        {
            if (mip->INODE.i_block[12] == 0) //Indirect block is null needs to be allocated
            {
                mip->INODE.i_block[12] = balloc(mip->dev);
                iput(mip);
                zero_block(mip->dev, mip->INODE.i_block[12]);
            }
            get_block(dev, mip->INODE.i_block[12], buf);
            lblk = lblk - 12;
            blk = ip[lblk]; //ip points to buf and treats it as buf of ints
            if (blk == 0)
            {
                ip[lblk] = balloc(mip->dev);
                put_block(dev, mip->INODE.i_block[12], buf);
                blk = ip[lblk];
                zero_block(mip->dev, blk);
            }
        }
        else //We assume double indirect (not implementing triple)
        {
            if (mip->INODE.i_block[13] == 0) //Double indirect is 0
            {
                mip->INODE.i_block[13] = balloc(mip->dev);
                iput(mip);
                zero_block(mip->dev, mip->INODE.i_block[13]);
            }
            get_block(dev, mip->INODE.i_block[13], buf); //Makes block of 256 indirects
            lblk = lblk - 256 - 12; //In order to find which block to grab in double indirect
            blk = ip[lblk / 256]; //Get the indirect block needed
            if (blk == 0) //needed Indirect block is 0
            {
                ip[lblk / 256] = balloc(mip->dev); //Allocate that indirect block
                put_block(dev, mip->INODE.i_block[13], buf);
                blk = ip[lblk / 256];
                zero_block(mip->dev, blk);
            }
            oldBlk = blk;
            get_block(dev, blk, buf); //Get indirect block contents
            lblk %= 256; //Offset in the indirect block
            blk = ip[lblk];
            if (blk == 0) //block wanted is 0
            { //Allocate and initialize it
                ip[lblk] = balloc(mip->dev);
                put_block(dev, oldBlk, buf);
                blk = ip[lblk];
                zero_block(mip->dev, blk);
            }
        }
        //Grabbed the blk now read data from the grabbed block
        get_block(dev, blk, kbuf);
        remainBytes = BLKSIZE - startByte;
        /************Do stuff to grabbed block ************** */
        ubuf = kbuf + startByte; //What we are writing to
        while (nbytes)
        {
            // put_ubyte(*cp++, *ubuf++);
            *ubuf = *cp;
            ubuf++;
            cp++;
            offset++;
            count++;
            remainBytes--;
            nbytes--;
            if (offset > fileSize)
                fileSize++;
            if (nbytes <= 0)
                break;
        }
        put_block(dev, blk, kbuf);
    }
    mip->INODE.i_size = fileSize;
    ft->offset = offset;
    mip->dirty = 1;
    return count; //Return how many bytes were read
}

/*!
    takes the block number (should already be allocated) and initializes
    all of it to zero
 */
int zero_block(int fd, int block_num)
{
    int i;
    char buf[BLKSIZE];
    for(i = 0; i < BLKSIZE; i++)
    {
        buf[i] = 0;
    }
    put_block(fd, block_num, buf);
}