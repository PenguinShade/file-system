#!/bin/bash

echo "Christian Burke" > output.txt
echo "Nicholas Fernandez" > output.txt
echo "Final Project" >> output.txt
echo -e "\n" >> output.txt

echo -e "********block.c Code*******" >> output.txt
echo -e "\n" >> output.txt
cat block.c >> output.txt

echo -e "********command.c Code*******" >> output.txt
echo -e "\n" >> output.txt
cat command.c >> output.txt

echo -e "********fileProcessing.c Code*******" >> output.txt
echo -e "\n" >> output.txt
cat fileProcessing.c >> output.txt

echo -e "********helper.c Code*******" >> output.txt
echo -e "\n" >> output.txt
cat helper.c >> output.txt

echo -e "********main.c Code*******" >> output.txt
echo -e "\n" >> output.txt
cat main.c >> output.txt

echo -e "********util.c Code*******" >> output.txt
echo -e "\n" >> output.txt
cat util.c >> output.txt