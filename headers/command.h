#include "fileProcessing.h"

int our_ls(char* pathname);
char * our_pwd(MINODE *wd);
int our_cp(char* source, char* target);
int our_cat(char* pathname);
int our_cd(char *pathname);
int our_mkdir(char *pathname);
int our_rmdir(char *pathname);
int our_creat(char *pathname);
int our_rm(char *pathname);
int our_touch(char *pathname);
int our_chmod(char *mode, char *pathname);
int our_open_file(char *pathname, char *mode_str);
int our_close_file(char *fd_str);
int print_fd();
int our_read_file(char *fd_str, char *nbytes_str);
int our_write_file(char *fd_str, char *in_buf);
int our_lseek_fd(char *fd, char *position);
int our_mv(char * src, char * dst);
int our_mount(char *fs, char *pathname);
int our_umount(char *fs);
