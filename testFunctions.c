#include "command.h"

/******************* For testing stuff ***************** */
//prints out all block numbers in the given block (number)
void printNumBlock(int blockNum)
{
    char buf[1024];
    uint32_t *i32p;
    get_block(dev, blockNum, buf);
    i32p = (uint32_t *)buf;
    while (*i32p != 0 && i32p < (uint32_t *)(buf + BLKSIZE))
    {
        printf("%d ", *i32p); //might print 8 byte int which would print things wierdly
        ++i32p;
    }
}

//Print an MINODE I think
int print(MINODE *mip)
{
  int blk;
  char buf[1024], temp[256];
  int i;
  DIR_ENTRY *dp;
  char *cp;

  INODE *ip = &mip->INODE;
  for (i=0; i < 12; i++){
    if (ip->i_block[i]==0)
      return 0;
    get_block(dev, ip->i_block[i], buf);

    dp = (DIR_ENTRY *)buf; 
    cp = buf;

    while(cp < buf+1024){

       // make dp->name a string in temp[ ]
       printf("%d %d %d %s\n", dp->inode, dp->rec_len, dp->name_len, temp);

       cp += dp->rec_len;
       dp = (DIR_ENTRY *)cp;
    }
  }
}