#include "block.h"

int our_link(char *file1Path, char *file2Path);
int our_unlink(char *pathname);
int our_symlink(char *file1path, char *file2path);
int our_stat(char *pathname);
int ls_file(MINODE *mip, char *name);
int ls_dir(MINODE *mip);
int rpwd(MINODE *wd, int childIno, char *buf);
int mount_root();
int load_mnt_entry(MNT_ENTRY *mnt);

